import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonContent, IonHeader, IonTitle, IonToolbar, IonButtons, IonBackButton } from '@ionic/angular/standalone';
import { ActivatedRoute } from '@angular/router';
import { PokemonService } from '../services/pokemon/pokemon.service';

@Component({
  selector: 'app-pokemon-details',
  templateUrl: './pokemon-details.page.html',
  styleUrls: ['./pokemon-details.page.scss'],
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    IonContent,
    IonHeader, 
    IonTitle, 
    IonToolbar,
    IonButtons,
    IonBackButton
  ]
})
export class PokemonDetailsPage implements OnInit {
  nomePokemon: string = "";
  id_pokemon: number = 0;
  tipo_pokemon: string = "";
  altura_pokemon: number = 0;
  peso_pokemon: number = 0;
  sprites_pokemon: any = {};

  constructor(
    private readonly actRoute: ActivatedRoute,
    private readonly service: PokemonService
  ) { }

  ngOnInit() {
    this.nomePokemon = this.actRoute.snapshot.params['nomePokemon'];
    this.LoadPokemon();
  }

  LoadPokemon() {
    this.service.getPokemonByName(this.nomePokemon).subscribe({
      next: (apiResponse: any) => {
        console.log(apiResponse);
        console.log(this.nomePokemon);

        this.id_pokemon = apiResponse.id;
        console.log(this.id_pokemon);

        if (apiResponse.types && apiResponse.types.length > 0) {
          this.tipo_pokemon = apiResponse.types.map((type: any) => type.type.name).join(', ');
        } else {
          this.tipo_pokemon = 'Desconhecido';
        }
        console.log(this.tipo_pokemon);

        this.altura_pokemon = apiResponse.height;
        console.log(this.altura_pokemon);

        this.peso_pokemon = apiResponse.weight;
        console.log(this.peso_pokemon);

        this.sprites_pokemon = apiResponse.sprites;
        console.log(this.sprites_pokemon);

      },
      error: (erro: any) => {
        console.error(erro);
      }
    });
  }
}
